using NUnit.Framework;
using HrDepartment.Application.Services;
using HrDepartment.Application.Interfaces;
using Moq;
using System;
using System.Threading.Tasks;
using HrDepartment.Domain.Entities;
using HrDepartment.Domain.Enums;

namespace HrDepartment.Application.Test
{
    public class JobSeekerRateService_Tests
    {
        private JobSeekerRateService _service;

        private Mock<ISanctionService> _sanctionService;

        private Mock<JobSeeker> _seeker;

        


        [SetUp]
        public void Setup()
        {
            _sanctionService = new Mock<ISanctionService>();

            _sanctionService.Setup(data => data.IsInSanctionsListAsync(It.IsAny<string>(), It.IsAny<string>(),
                                                                        It.IsAny<string>(), It.IsAny<DateTime>()))
                                                .Returns(Task<bool>.Run(()=> { return false; }));
            
            
        }

        [Test]
        public void JobRateSeekerTest_SanctionServiceArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new JobSeekerRateService(null));
        }

        [Test]
        public void JobRateSeekerTest_SanctionServiceIsNotNul()
        {
            _service = new JobSeekerRateService(_sanctionService.Object);

            Assert.IsNotNull(_service);
        }

        [Test]
        public void JobRateSeekerTest_CalculateJobSeekerRating_MaxRating()
        {
            _seeker = new Mock<JobSeeker>();
            _seeker.Object.BirthDate = new DateTime(1993, 1, 22);
            _seeker.Object.Education = EducationLevel.University;
            _seeker.Object.Experience = 11;
            _seeker.Object.BadHabits = BadHabits.None;

            _service = new JobSeekerRateService(_sanctionService.Object);

            var rate = _service.CalculateJobSeekerRatingAsync(_seeker.Object).Result;

            Assert.AreEqual(95, rate);
        }

        [Test]
        public void JobRateSeekerTest_CalculateJobSeekerRating_DrugsWithNoEducationAndExperienceIsNotCool()
        {
            _seeker = new Mock<JobSeeker>();
            _seeker.Object.BirthDate = new DateTime(1993, 1, 22);
            _seeker.Object.Education = EducationLevel.None;
            _seeker.Object.Experience = 0;
            _seeker.Object.BadHabits = BadHabits.Drugs;

            _service = new JobSeekerRateService(_sanctionService.Object);

            var rate = _service.CalculateJobSeekerRatingAsync(_seeker.Object).Result;

            Assert.AreEqual(0, rate);
        }

        [Test]
        public void JobRateSeekerTest_CalculateJobSeekerRating_NotRealEducation_ArgumentsOutOfRangeException()
        {
            _seeker = new Mock<JobSeeker>();
            _seeker.Object.BirthDate = new DateTime(1993, 1, 22);
            _seeker.Object.Experience = 0;
            _seeker.Object.BadHabits = BadHabits.None;
            _seeker.Object.Education = EducationLevel.University;
            _seeker.Object.Education += 1;

            _service = new JobSeekerRateService(_sanctionService.Object);

            Assert.ThrowsAsync<ArgumentOutOfRangeException>(() => _service.CalculateJobSeekerRatingAsync(_seeker.Object));
        }
        [Test]
        public void JobRateSeekerTest_CalculateJobSeekerRating_ExpectedTrueRating()
        {
            _seeker = new Mock<JobSeeker>();
            _seeker.Object.BirthDate = new DateTime(1975, 1, 22);
            _seeker.Object.Education = EducationLevel.School;
            _seeker.Object.Experience = 6;
            _seeker.Object.BadHabits = BadHabits.Smoking;

            _service = new JobSeekerRateService(_sanctionService.Object);

            var rate = _service.CalculateJobSeekerRatingAsync(_seeker.Object).Result;

            Assert.AreEqual(45, rate);

            _seeker = new Mock<JobSeeker>();
            _seeker.Object.BirthDate = new DateTime(1920, 1, 22);
            _seeker.Object.Education = EducationLevel.College;
            _seeker.Object.Experience = 2;
            _seeker.Object.BadHabits = BadHabits.Alcoholism;

            _service = new JobSeekerRateService(_sanctionService.Object);

            var rate1 = _service.CalculateJobSeekerRatingAsync(_seeker.Object).Result;

            Assert.AreEqual(20, rate1);

            _seeker = new Mock<JobSeeker>();
            _seeker.Object.BirthDate = new DateTime(2020, 1, 22);
            _seeker.Object.Education = EducationLevel.None;
            _seeker.Object.Experience = 3;
            _seeker.Object.BadHabits = BadHabits.None;

            _service = new JobSeekerRateService(_sanctionService.Object);

            var rate2 = _service.CalculateJobSeekerRatingAsync(_seeker.Object).Result;

            Assert.AreEqual(25, rate2);
        }

        [Test]
        public void JobRateSeekerTest_CalculateJobSeekerRating_SomeSanctionAboutSeeker()
        {
            _sanctionService.Setup(data => data.IsInSanctionsListAsync(It.IsAny<string>(), It.IsAny<string>(),
                                                                        It.IsAny<string>(), It.IsAny<DateTime>()))
                                                .Returns(Task<bool>.Run(() => { return true; }));

            _seeker = new Mock<JobSeeker>();
            _seeker.Object.BirthDate = new DateTime(1993, 1, 22);
            _seeker.Object.FirstName = "FirstName";
            _seeker.Object.LastName = "LastName";
            _seeker.Object.MiddleName = "MiddleName";

            _service = new JobSeekerRateService(_sanctionService.Object);

            var rate = _service.CalculateJobSeekerRatingAsync(_seeker.Object).Result;

            Assert.AreEqual(0, rate);
        }
    }
}